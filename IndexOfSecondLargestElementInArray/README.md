## bài tập 
  hoàn thành hàm ```IndexOfSecondLargestElementInArray```
  - với tham số đầu vào hàm là mảng số nguyên X
  - kết quả hàm sau khi thực thi là vị trí(index) của SỐ-LỚN-THỨ-2-TRONG-MẢNG
  - trong trường hợp tồn tại 2 số có cùng giá trị thì: số có index nhỏ hơn là số lớn hơn
  ví dụ 1: X = [5, 4, 3, 2, 1, 0]
  => 4 là giá trị lớn thứ 2
  => kế quả hàm sẽ là 1(là vị trí của số lớn thứ 2)

  ví dụ 2: X = [5]
  => không có số có giá trị lớn thứ 2
  => kế quả hàm sẽ là -1

  ví dụ 3: X = [1, 5, 5, 5]
  => phần tử ở vị trí index = 1 là lớn nhất
  => phần tử ở vị trí index = 2 là số lớn thứ 2
  => kế quả hàm sẽ là 2(là vị trí của số lớn thứ 2)

  ví dụ 4: X = [10, 5, 5, 5]
  => phần tử ở vị trí index bằng 0 là lớn nhất
  => phần tử ở vị trí index = 1 là số lớn thứ 2
  => kế quả hàm sẽ là 1(là vị trí của số lớn thứ 2)

## lưu ý: khi nộp bài source code phải luôn luôn build thành công.

## kiểm tra kết quả của mình bằng cách chạy unit test
	trên thanh menu của Visual studio -> Test -> Test Explorer -> chạy test để kiểm tra kết quả
